# setdefault() sample
spam = {'name':'Pooka', 'age':5}

spam.setdefault('color', 'black')
print(spam)

# when a second instance of setdefault() is called needs to be with 
# a different key, otherwise the value is not going to change
spam.setdefault('color', 'white')
print(spam)