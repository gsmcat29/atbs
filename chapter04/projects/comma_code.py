'''
Say you have a list value like this:
spam = ['apples', 'bananas', 'tofu', 'cats']

Write a function that takes a list value as an argument and returns a string
with all the items separated by a comma and a space, with and inserted
before the last item. For example, passing the previous spam list to the
function would return 'apples, bananas, tofu, and cats'. But your function
should be able to work with any list value passed to it. Be sure to test the case
where an empty list [] is passed to your function.
'''

def comma_func(list_name):
    pass
    new_string = ''
    for elem in list_name:
        if elem == list_name[-1]:
            print('and ' + elem)
        else:
            print(elem, end=', ')
        #print(elem)


spam = ['apples', 'bananas', 'tofu', 'cats']
#seal = ['fluffy', 'cold']

comma_func(spam)
#comma_func(seal)
print()