'''
Write a program to find out how often a streak of six heads or a streak of
six tails comes up in a randomly generated list of heads and tails. Your program breaks up the experiment into two parts: the first part generates a
list of randomly selected 'heads' and 'tails' values, and the second part checks
if there is a streak in it. Put all of this code in a loop that repeats the
experiment 10,000 times so we can find out what percentage of the coin flips
contains a streak of six heads or tails in a row. As a hint, the function call
random.randint(0, 1) will return a 0 value 50% of the time and a 1 value the
other 50% of the time.
You can start with the following template:
--------------------------------------------------------------------------------
import random
numberOfStreaks = 0
for experimentNumber in range(10000):
    # Code that creates a list of 100 'heads' or 'tails' values.

    # Code that checks if there is a streak of 6 heads or tails in a row.

print('Chance of streak: %s%%' % (numberOfStreaks / 100))
--------------------------------------------------------------------------------
Of course, this is only an estimate, but 10,000 is a decent sample size.
Some knowledge of mathematics could give you the exact answer and save
you the trouble of writing a program, but programmers are notoriously bad
at math
'''

import random

number_of_streaks = 0
head_count = 0
tail_count = 0
list_of_coin = []

for experimentNumber in range(10000):
	# Code that creates a list of 100 'heads' or 'tails' values.
	list_of_coin = []
	headCount = 0

	for x in range(100):
		valueOfCoin = random.randint(0, 1)
		if valueOfCoin == 0:
			list_of_coin.append('H')
			head_count += 1
			tail_count = 0
		else:
			list_of_coin.append('T')
			tail_count += 1
			head_count = 0


	# Code that checks if there is a streak of 6 heads or tails in a row.
	if head_count == 6:
		number_of_streaks += 1
	elif tail_count == 6:
		number_of_streaks += 1


print(list_of_coin)
print('# of streak: ', number_of_streaks)
print('Chance of streak: %s%%' % (number_of_streaks / 100))