3. Write out the truth tables of each Boolean operator (that is, every
possible combination of Boolean values for the operator and what they
evaluate to).


not (!) =====================
value       result
not True    False
not False   True


or (||) =====================
val1    val2       result
False   False       False
False   True        True
True    False       True
True    True        True

and (&&) ====================
val1    val2    result
False   False   False
False   True    False
True    False   False
True    True    True



